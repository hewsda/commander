<?php

declare(strict_types=1);

namespace Hewsda\Commander\Providers;

use Illuminate\Support\ServiceProvider;
use Prooph\ServiceBus\CommandBus;
use Prooph\ServiceBus\EventBus;
use Prooph\ServiceBus\QueryBus;

class CommanderServiceProvider extends ServiceProvider
{
    protected $namespace = 'commander';

    public function boot()
    {
        // fixMe config_path
        $this->publishes([
            __DIR__ . '../../config/commander.php' => config_path('commander.php')
        ], 'commander');
    }

    public function register()
    {
        $this->registerConfig();

        $this->registerBus();
    }

    protected function registerConfig()
    {
        $this->mergeConfigFrom(__DIR__ . '/../../config/commander.php', 'commander');
    }

    private function registerBus()
    {
        foreach ($this->getBus() as $key => $class) {
            $this->app->singleton($class);

            $plugins = $this->app['config']->get('commander.' . $this->namespace . '.service_bus.' . $key . '_bus.plugins');
            foreach ($plugins as $plugin) {
                $this->app->bindIf($plugin);
                $this->app[$plugin]->attachToMessageBus($this->app[$class]);
            }
        }
    }

    private function getBus(): array
    {
        return [
            'command' => CommandBus::class,
            'event' => EventBus::class,
            'query' => QueryBus::class
        ];
    }
}