<?php

declare(strict_types=1);

namespace Hewsda\Commander\Providers;

use Illuminate\Support\ServiceProvider;
use Prooph\ServiceBus\CommandBus;
use Prooph\ServiceBus\EventBus;
use Prooph\ServiceBus\Plugin\Router\CommandRouter;
use Prooph\ServiceBus\Plugin\Router\EventRouter;
use Prooph\ServiceBus\Plugin\Router\QueryRouter;
use Prooph\ServiceBus\QueryBus;

class RouterServiceProvider extends ServiceProvider
{
    protected $namespace = 'commander';

    /**
     * @var array
     */
    protected $routers = [
        'command' => CommandRouter::class,
        'event' => EventRouter::class,
        'query' => QueryRouter::class
    ];

    public function register()
    {
        foreach ($this->routers as $key => $class) {

            $routes = $this->getRoutes($key);

            $this->registerRoutes($this->flatRoutes($routes, $key));

            // Attach each router to his bus.
            $instance = new $class($routes);

            switch ($key) {
                case 'command':
                    $instance->attachToMessageBus($this->app[CommandBus::class]);
                    break;
                case 'event':
                    $instance->attachToMessageBus($this->app[EventBus::class]);
                    break;
                case 'query':
                    $instance->attachToMessageBus($this->app[QueryBus::class]);
                    break;
            }
        }
    }

    protected function registerRoutes(array $routes): void
    {
        foreach ($routes as $route) {
            $this->app->bindIf($route);
        }
    }

    protected function getRoutes(string $key): array
    {
        return $this->app['config']->get('commander.' . $this->namespace . '.service_bus.' . $key . '_bus.router.routes');
    }

    protected function flatRoutes(array $routes, string $busType): array
    {
        $router = [];
        foreach ($routes as $route) {
            if (is_array($route)) {
                foreach ($route as $key => $value) {
                    $router[] = $key;
                    $value = is_array($value) ? array_flatten($value) : [$value];
                    $router[] = $value;
                }
            }

            if (is_string($route)) {
                $router[] = $route;
            }
        }

        return array_flatten($router);
    }
}