<?php

declare(strict_types=1);

namespace Hewsda\Commander\Plugin;

use Prooph\Common\Event\ActionEvent;
use Prooph\Common\Messaging\HasMessageName;
use Prooph\ServiceBus\EventBus;
use Prooph\ServiceBus\MessageBus;
use Prooph\ServiceBus\Plugin\AbstractPlugin;

class OnEventNameStrategy extends AbstractPlugin
{

    public function attachToMessageBus(MessageBus $messageBus): void
    {
        $this->listenerHandlers[] = $messageBus->attach(
            MessageBus::EVENT_DISPATCH,
            function (ActionEvent $actionEvent): void {
                $message = $actionEvent->getParam(MessageBus::EVENT_PARAM_MESSAGE);
                $handlers = $actionEvent->getParam(EventBus::EVENT_PARAM_EVENT_LISTENERS, []);

                foreach ($handlers as $handler) {
                      $this->onEventName($handler, $message);
                }

                $actionEvent->setParam(MessageBus::EVENT_PARAM_MESSAGE_HANDLED, true);
            },
            MessageBus::PRIORITY_INVOKE_HANDLER
        );
    }

    private function onEventName($handler, $message): void
    {
        $method = 'on' . $this->determineEventHandler($message);

        $handler->{$method}($message);
    }

    private function determineEventHandler($event): string
    {
        $eventName = ($event instanceof HasMessageName)
            ? $event->messageName() : is_object($event)
                ? get_class($event) : gettype($event);

        return implode('', array_slice(explode('\\', $eventName), -1));
    }
}