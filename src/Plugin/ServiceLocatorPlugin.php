<?php

declare(strict_types=1);

namespace Hewsda\Commander\Plugin;

use Illuminate\Contracts\Container\Container;
use Prooph\Common\Event\ActionEvent;
use Prooph\ServiceBus\EventBus;
use Prooph\ServiceBus\MessageBus;
use Prooph\ServiceBus\Plugin\AbstractPlugin;

class ServiceLocatorPlugin extends AbstractPlugin
{
    /**
     * @var Container
     */
    private $serviceLocator;

    /**
     * ServiceLocatorPlugin constructor.
     *
     * @param Container $serviceLocator
     */
    public function __construct(Container $serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
    }

    public function attachToMessageBus(MessageBus $messageBus): void
    {
        $this->listenerHandlers[] = $messageBus->attach(
            MessageBus::EVENT_DISPATCH,
            function (ActionEvent $actionEvent): void {
                $messageHandlerAlias = $actionEvent->getParam(MessageBus::EVENT_PARAM_MESSAGE_HANDLER);

                if (is_string($messageHandlerAlias) && $this->serviceLocator->bound($messageHandlerAlias)) {
                    $actionEvent->setParam(MessageBus::EVENT_PARAM_MESSAGE_HANDLER, $this->serviceLocator->make($messageHandlerAlias));
                }

                // for event bus only
                $eventListeners = [];
                foreach ($actionEvent->getParam(EventBus::EVENT_PARAM_EVENT_LISTENERS, []) as $eventListenerAlias) {
                    if (is_string($eventListenerAlias) && $this->serviceLocator->bound($eventListenerAlias)) {
                        $eventListeners[] = $this->serviceLocator->make($eventListenerAlias);
                    }
                }
                if (! empty($eventListeners)) {
                    $actionEvent->setParam(EventBus::EVENT_PARAM_EVENT_LISTENERS, $eventListeners);
                }
            },
            MessageBus::PRIORITY_LOCATE_HANDLER
        );
    }
}