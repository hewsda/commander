<?php

return [
    'commander' => [
          'service_bus' => [
            'command_bus' => [
                'plugins' => [],
                'router' => [
                    'routes' => [],
                ],
            ],
            'event_bus' => [
                'plugins' => [],
                'router' => [
                    'routes' => [],
                ],
            ],
            'query_bus' => [
                'plugins' => [],
                'router' => [
                    'routes' => [],
                ],
            ],
        ],
    ],
];